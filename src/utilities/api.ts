import { HttpService } from "./HttpService";

export const ENDPOINT = {
  GET_USER: "user",
  ACTIVATE_USER: "user/activate",
  GENERATE_USER: "user/generate",
  SIGNIN: "signin",
  SIGNIN_STOCKIES: "signin/stockies",
  DOWNLINES: "user/downline",
  GET_TRANSACTION_BY_USER: "transaction",
  GET_TRANSACTION_BY_STOCKIES: "transaction/stockies",
  CREATE_TRANSACTION: "transaction/create",
  GET_MONTHLY_BONUS: "bonus",
  GET_MONTHLY_LEVEL_BONUS: "bonus-level",
  GET_ALL_DOWNLINES: "user/downline/all",
  GET_PRODUCT: "product",
  GET_STOCKIES: "stockies",
  GET_STOCKIES_DATA: "stockies/data",
  GET_STOCKIES_PURCHASE: "stockies-purchase",
  GET_USER_BY_STOCKIES: "user/by-stockies",
  GET_USER_BONUS_BY_STOCKIES: "bonus/by-stockies",
  CREATE_STOCKIES_PURCHASE: "stockies-purchase/create",
  GET_STOCK: "stock",
  GET_PROVINCE: "province",
  GET_REGENCY: "regency",
  GET_DISTRICT: "district",
  GET_VILLAGE: "village",
};

export async function getAllUser() {
  const res = await HttpService.get(ENDPOINT.GET_USER, null, {
    "Content-Type": "application/json",
  });

  if (res.data && res.data.statusCode === 200) {
    return res.data.message;
  }
  return "error when get all user data";
}
