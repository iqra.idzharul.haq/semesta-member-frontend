export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "fa fa-tachometer",
    },
    {
      name: "Peta Jaringan",
      url: "/map",
      icon: "fa fa-users",
    },
    {
      title: true,
      name: "Bonus",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {}, // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "", // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Bonus Belanja",
      url: "/monthly-purchase-bonus",
      icon: "fa fa-shopping-cart",
    },
    {
      name: "Bonus Level",
      url: "/monthly-level-bonus",
      icon: "fa fa-sort-amount-asc",
    },
  ],
};
