import React, { Component } from "react";
import { Container } from "reactstrap";

class Avatar extends Component<any, any> {
  render() {
    const { id, name } = this.props;
    return (
      <Container className="text-align-center mar-top-15">
        <i className="fa fa-user-circle fa-4x mar-bot-15"></i>
        <h6>{name ? name : "MEMBER NAME"}</h6>
        <p>{id ? id : "MEMBER ID"}</p>
      </Container>
    );
  }
}

export default Avatar;
