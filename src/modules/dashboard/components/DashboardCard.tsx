import React, { Component} from 'react';
import { Card, CardBody, Row, Col, Label } from 'reactstrap';

class DashboardCard extends Component<any, any> {
  render() {
    const { title, value, description, icon, primary, tutupPoin} = this.props
    return (
      <Card className={primary? "dashboard-card-primary": "dashboard-card-secondary"}>
        <CardBody>
          <Row>
            <Col md="8" lg="8">
              <Label>{title}</Label>
            </Col>   
            <Col md="4" lg="4">
              <i className={icon}></i>
            </Col>
          </Row>
          <Row>
            <Col>
              <h3 className={tutupPoin? "bolder pad-6" : "bolder pad-6 alert"}>{value}</h3>
            </Col>
          </Row>
          <Row>
            <Col>
              <h5 className="bolder pad-6 alert">{description}</h5>
            </Col>
          </Row>
        </CardBody>
      </Card>
    );
  }
}

export default DashboardCard;
