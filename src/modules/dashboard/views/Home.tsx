import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Label,
  CardHeader,
  Card,
  CardBody,
} from "reactstrap";
import DashboardCard from "../components/DashboardCard";
import {
  getLatestTransaction,
  getMonthlyBonus,
  getAllDownlines,
  getDirectDownlines,
  getMonthlyLevelBonus,
} from "../helper/api";
import { formatCurrency } from "../../../utilities/helper";
import LatestTransaction from "../components/LatestTransaction";
import BonusTable from "../components/BonusTable";
import { setMap } from "../../../utilities/localStorage";
import Tree from "react-vertical-tree";
import { TutupPoin } from "../../../config";

class DashboardHome extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      latestTransactions: [],
      totalTransactions: 0,
      bonus: { totalBonus: 0, level0: 0 },
      levelBonus: { totalBonus: 0, level0: 0 },
      totalRelation: 0,
      alert,
      titlePurchase: "Bonus Belanja",
      titleLevel: "Bonus Level",
      user: [
        {
          id: 1,
          name: "1",
          parent: null,
          children: [],
          totalBonus: 0,
          branch: 0,
        },
      ],
    };
  }

  async componentDidMount() {
    const res = await getLatestTransaction();
    const resBonus = await getMonthlyBonus();
    const resLevelBonus = await getMonthlyLevelBonus();
    const resMap = await getAllDownlines();
    const resDirectDownline = await getDirectDownlines();
    if (!res.error) {
      this.setState({ latestTransactions: res.data.content });
      this.setState({ totalTransactions: res.data.totalElements });
    } else {
      this.setState({ latestTransactions: [] });
    }
    if (!resBonus.error) {
      this.setState({ bonus: resBonus.data[0] });
    } else {
      console.log(resBonus.errorMessage);
    }
    if (!resLevelBonus.error) {
      this.setState({ levelBonus: resLevelBonus.data[0] });
    } else {
      console.log(resLevelBonus.errorMessage);
    }
    if (!resMap.error) {
      this.setState({ totalRelation: resMap.data.count });
      setMap(resMap.data.user);
    } else {
      console.log(resMap.errorMessage);
    }
    if (!resDirectDownline.error) {
      this.setState({ user: resDirectDownline.data.user });
    } else {
      console.log(resDirectDownline.errorMessage);
    }
  }

  render() {
    const {
      latestTransactions,
      totalTransactions,
      bonus,
      levelBonus,
      totalRelation,
      user,
      titlePurchase,
      titleLevel,
    } = this.state;
    let tutupPoin = false;
    if (bonus && bonus.level0 >= 1500) {
      tutupPoin = true;
    }
    return (
      <Container>
        <Row>
          <Col>
            <DashboardCard
              title="TOTAL JARINGAN"
              value={totalRelation + " Member"}
              tutupPoin={true}
              icon="fa fa-users fa-2x align-self-flex-end"
              primary={true}
            />
          </Col>
          <Col>
            <DashboardCard
              title="TOTAL TRANSAKSI"
              value={totalTransactions}
              tutupPoin={true}
              icon="fa fa-book fa-2x align-self-flex-end"
              primary={true}
            />
          </Col>
          <Col>
            <DashboardCard
              title="TOTAL BONUS"
              value={
                bonus
                  ? bonus.totalBonus
                    ? formatCurrency(bonus.totalBonus + levelBonus.totalBonus)
                    : formatCurrency(0)
                  : formatCurrency(0)
              }
              description={
                bonus
                  ? bonus.level0 >= TutupPoin
                    ? ""
                    : "Anda belum tutup poin ( -" +
                      (TutupPoin - bonus.level0) +
                      " )"
                  : "Anda belum tutup poin ( -" + TutupPoin + " )"
              }
              tutupPoin={tutupPoin}
              icon="fa fa-money fa-2x align-self-flex-end"
              primary={false}
            />
          </Col>
        </Row>
        <Row>
          <Col lg="8" md="8">
            <Card>
              <CardHeader>Rincian Bonus Belanja</CardHeader>
              <CardBody>
                <Row className="mar-bot-30">
                  <Col>
                    <Tree
                      data={user}
                      render={(item: any) => (
                        <Button
                          color={item.id ? "primary" : "secondary"}
                          className="min-width-100 pad-8-13"
                          onClick={() => {}}
                        >
                          <Label className="bold">
                            {item.branch
                              ? "Jalur " + item.branch
                              : "Belanja Pribadi"}
                          </Label>
                          <br />
                          {item.id ? formatCurrency(item.totalBonus) : "-"}
                        </Button>
                      )}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <div className="text-align-right mar-bot-15 pad-3-13">
                      <span
                        className="pointer"
                        onClick={() => {
                          this.props.history.push("/map");
                        }}
                      >
                        Lihat Peta Jaringan Lengkap &nbsp;&nbsp;{" "}
                        <i className="fa fa-caret-right" />
                      </span>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <Col lg="4" md="4">
            <BonusTable
              title={titlePurchase}
              bonus={bonus}
              onDetailedClick={() => {
                this.props.history.push("/monthly-purchase-bonus");
              }}
            />
            <BonusTable
              title={titleLevel}
              bonus={levelBonus}
              onDetailedClick={() => {
                this.props.history.push("/monthly-level-bonus");
              }}
            />
          </Col>
        </Row>
        <Row>
          <Col lg="12" md="12">
            <LatestTransaction latestTransactions={latestTransactions} />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default DashboardHome;
