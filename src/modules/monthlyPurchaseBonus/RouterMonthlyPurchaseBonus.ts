import React from "react";

const routes = [
  {
    path: "/monthly-purchase-bonus",
    exact: true,
    name: "Bonus Belanja",
    component: React.lazy(() => import("./views/MonthlyPurchaseBonusSummary")),
  },
];

export default routes;
