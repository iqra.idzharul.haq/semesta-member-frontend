import React, { Component, Fragment } from "react";
import { Col, Form, Label, Input, Row } from "reactstrap";

class SearchMutationForm extends Component<any, any> {
  onChange = (e: any) => {
    const { filter } = this.props;
    const copyFilter = filter;
    copyFilter[e.target.name] = e.target.value;
    this.props.setStateByName("filter", copyFilter);
  };
  render() {
    const { filter } = this.props;
    return (
      <Fragment>
        <Col xs="12">
          <Form>
            <Row className="mar-bot-5">
              <Col md="2" lg="2">
                <Label>Periode</Label>
              </Col>
              <Col md="3" lg="3">
                <Input
                  name="period"
                  type="select"
                  value={filter.period}
                  onChange={this.onChange}
                >
                  <option value="">-- Pilih Periode --</option>
                  <option value="AUG-20">AUG-20</option>
                </Input>
              </Col>
            </Row>
          </Form>
        </Col>
      </Fragment>
    );
  }
}

export default SearchMutationForm;
