import React, { Component } from 'react';
import { Button, Container, Label, Col, Row } from 'reactstrap';
import { getDirectDownline } from '../helper/api';

class MemberNode extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      user: props.user,
      level: props.level,
      showDownline: false,
      hover: false,
    };
  }
  
  onClick = async () =>{
    this.setState({showDownline: true})
  }

  handleMouseIn() {
    this.setState({ hover: true })
  }
  
  handleMouseOut() {
    this.setState({ hover: false })
  }


  render() { 
    const tooltipStyle = {
      color: this.state.hover ? '#000000' : '#00000000'
    }
    
    const {user, level, showDownline} = this.state;
    return (
      <div className="animated fadeIn">
        {/* <Label>Level {level} &nbsp;&nbsp;&nbsp;</Label> */}
        <div>
          <div style={tooltipStyle}>{user.fullName}</div>
        </div>
        {
          user && user.downLines && <Button onMouseOver={this.handleMouseIn.bind(this)} onMouseOut={this.handleMouseOut.bind(this)} color={user.downLines.length ? "primary":"danger"} className='mar-bot-15' onClick={this.onClick}>{level}</Button>
        }
        
        <Row>
        {
          showDownline && user.downLines && level <10  && user.downLines.map((downline: any) =>{
            const { id } = downline;
            return <Col>
              <MemberNode key={id} user={downline} level={level+1} />
            </Col>
          })
        }
        </Row>
     </div>
    )
  }
}

export default MemberNode;