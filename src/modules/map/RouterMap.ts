import React from "react";

const routes = [
  {
    path: "/map",
    exact: true,
    name: "Peta Jaringan",
    component: React.lazy(() => import("./views/MapSummary")),
  },
];

export default routes;
