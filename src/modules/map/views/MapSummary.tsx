import React, { Component } from 'react';
import { Button, Table } from 'reactstrap';
import { getMap } from '../../../utilities/localStorage';
import Tree from 'react-vertical-tree'
import { formatCurrency } from '../../../utilities/helper';

class MapSummary extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      alert: null,
      user: [
        {id: 1, name: '1', parent: null, children: [], purchaseBonus: 0}
      ],
    };
  }

  async componentDidMount(){
    const map = await getMap()
    console.log(map)
    this.setState({user: map})
  }

  render() { 
    const { alert, user } = this.state;
    return (
      <div className="animated fadeIn mar-bot-30">
        {alert}
        <Table borderless responsive hover size="sm">
          <tr>
            <td className="text-align-center">
              <Tree data={user} render={ (item: any) => 
                <Button className="min-width-140 pad-8-13" color={item.purchaseBonus? "success": "primary"} onClick={()=>{}}>
                  Level {item.level}
                  <br/>
                  {item.name}
                  <br/>
                  <div className="bold">{item.id? formatCurrency(item.purchaseBonus): "-"}</div>
                </Button>
                }
              />
            </td>
          </tr>
        </Table>
      </div>
    )
  }
}

export default MapSummary;