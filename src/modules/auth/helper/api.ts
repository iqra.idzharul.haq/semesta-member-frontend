import { HttpService } from "../../../utilities/HttpService";
import { ENDPOINT } from "../../../utilities/api";

// export async function getStockiesListAPI() {
//   const res = await HttpService.get(ENDPOINT.GET_STOCKIES, null, {
//     "Content-Type": "application/json",
//   });

//   if (res.data && res.data.statusCode === 200) {
//     return {
//       error: false,
//       message: res.data.message,
//     };
//   }
//   return {
//     error: true,
//     messsage: "terjadi kesalahan ketika mengambil data stokis",
//   };
// }

export async function getProvinceListAPI() {
  const res = await HttpService.get(ENDPOINT.GET_PROVINCE, null, {
    "Content-Type": "application/json",
  });

  if (res.data && res.data.statusCode === 200) {
    return {
      error: false,
      message: res.data.message,
    };
  }
  return {
    error: true,
    messsage: "terjadi kesalahan ketika mengambil data provinsi",
  };
}

export async function getRegencyListAPI(provinceId: string) {
  const res = await HttpService.get(
    ENDPOINT.GET_REGENCY + "/" + provinceId,
    null,
    {
      "Content-Type": "application/json",
    }
  );

  if (res.data && res.data.statusCode === 200) {
    return {
      error: false,
      message: res.data.message,
    };
  }
  return {
    error: true,
    messsage: "terjadi kesalahan ketika mengambil data kota/kabupaten",
  };
}

export async function getDistrictListAPI(id: string) {
  const res = await HttpService.get(ENDPOINT.GET_DISTRICT + "/" + id, null, {
    "Content-Type": "application/json",
  });

  if (res.data && res.data.statusCode === 200) {
    return {
      error: false,
      message: res.data.message,
    };
  }
  return {
    error: true,
    messsage: "terjadi kesalahan ketika mengambil data kecamatan",
  };
}

export async function getVillageListAPI(id: string) {
  const res = await HttpService.get(ENDPOINT.GET_VILLAGE + "/" + id, null, {
    "Content-Type": "application/json",
  });

  if (res.data && res.data.statusCode === 200) {
    return {
      error: false,
      message: res.data.message,
    };
  }
  return {
    error: true,
    messsage: "terjadi kesalahan ketika mengambil data desa/kelurahan",
  };
}

export async function getStockiesListAPI(id: string) {
  const res = await HttpService.get(
    ENDPOINT.GET_STOCKIES + "/village/" + id,
    null,
    {
      "Content-Type": "application/json",
    }
  );

  if (res.data && res.data.statusCode === 200) {
    return {
      error: false,
      message: res.data.message,
    };
  }
  return {
    error: true,
    messsage: "terjadi kesalahan ketika mengambil data desa/kelurahan",
  };
}
