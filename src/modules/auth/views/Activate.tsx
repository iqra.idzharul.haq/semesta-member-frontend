import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import { HttpService } from "../../../utilities/HttpService";
import { SuccessAlert, DangerAlert } from "../../../components/sweet-alert";
import { ENDPOINT } from "../../../utilities/api";
import {
  getStockiesListAPI,
  getProvinceListAPI,
  getRegencyListAPI,
  getDistrictListAPI,
  getVillageListAPI,
} from "../helper/api";

class Activate extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: {
        username: "",
        fullname: "",
        email: "",
        upliner: "",
        key: "",
        stockies: "HQ001",
        password: "",
        confirmPassword: "",
        address: "",
        sponsor: "",
        province: "",
        regency: "",
        district: "",
        village: "",
      },
      stockiesList: [{ username: "HQ001", name: "HEADQUARTER" }],
      provinceList: [{ id: "", name: "-- PILIH PROVINSI --" }],
      regencyList: [{ id: "", name: "-- PILIH KABUPATEN/KOTA --" }],
      districtList: [{ id: "", name: "-- PILIH KECAMATAN --" }],
      villageList: [{ id: "", name: "-- PILIH DESA/KELURAHAN --" }],
      alert: null,
    };
  }

  componentDidMount() {
    // this.getStockiesList();
    this.getProvinceList();
  }

  async getProvinceList() {
    const resProvince = await getProvinceListAPI();
    if (!resProvince.error) {
      this.setState({
        provinceList: [
          { id: "", name: "-- PILIH PROVINSI --" },
          ...resProvince.message,
        ],
      });
    } else {
      this.setState({
        provinceList: [{ id: "", name: "-- PILIH PROVINSI --" }],
      });
    }
  }

  async getRegencyList(id: string) {
    const res = await getRegencyListAPI(id);
    if (!res.error) {
      this.setState({
        regencyList: [
          { id: "", name: "-- PILIH KABUPATEN/KOTA --" },
          ...res.message,
        ],
      });
    } else {
      this.setState({
        regencyList: [{ id: "", name: "-- PILIH KABUPATEN/KOTA --" }],
      });
    }
  }

  async getDistrictList(id: string) {
    const res = await getDistrictListAPI(id);
    if (!res.error) {
      this.setState({
        districtList: [
          { id: "", name: "-- PILIH KECAMATAN --" },
          ...res.message,
        ],
      });
    } else {
      this.setState({
        districtList: [{ id: "", name: "-- PILIH KECAMATAN --" }],
      });
    }
  }

  async getVillageList(id: string) {
    const res = await getVillageListAPI(id);
    if (!res.error) {
      this.setState({
        villageList: [
          { id: "", name: "-- PILIH DESA/KELURAHAN --" },
          ...res.message,
        ],
      });
    } else {
      this.setState({
        villageList: [{ id: "", name: "-- PILIH DESA/KELURAHAN --" }],
      });
    }
  }

  async getStockiesList(id: string) {
    const res = await getStockiesListAPI(id);
    console.log(res);
    if (!res.error) {
      this.setState({
        stockiesList: [
          { username: "HQ001", name: "HEADQUARTER" },
          ...res.message,
        ],
      });
    } else {
      this.setState({
        stockiesList: [{ username: "HQ001", name: "HEADQUARTER" }],
      });
    }
  }

  onChange = (e: any) => {
    const { data, stockiesList } = this.state;
    const copyData = { ...data };
    let value = e.target.value;
    if (
      e.target.name === "username" ||
      e.target.name === "upliner" ||
      e.target.name === "sponsor" ||
      e.target.name === "key"
    )
      value = value.toUpperCase();
    copyData[e.target.name] = value;
    if (e.target.name === "province") {
      if (e.target.value) this.getRegencyList(e.target.value);
      else {
        this.setState({
          regencyList: [{ id: "", name: "-- PILIH KABUPATEN/KOTA --" }],
          districtList: [{ id: "", name: "-- PILIH KECAMATAN --" }],
          villageList: [{ id: "", name: "-- PILIH DESA/KELURAHAN --" }],
        });
      }
      copyData.regency = "";
      copyData.district = "";
      copyData.village = "";
      copyData.stockies = stockiesList[0].username;
    }
    if (e.target.name === "regency") {
      if (e.target.value) this.getDistrictList(e.target.value);
      else {
        this.setState({
          districtList: [{ id: "", name: "-- PILIH KECAMATAN --" }],
          villageList: [{ id: "", name: "-- PILIH DESA/KELURAHAN --" }],
        });
      }
      copyData.district = "";
      copyData.village = "";
      copyData.stockies = stockiesList[0].username;
    }
    if (e.target.name === "district") {
      if (e.target.value) this.getVillageList(e.target.value);
      else {
        this.setState({
          villageList: [{ id: "", name: "-- PILIH DESA/KELURAHAN --" }],
        });
      }
      copyData.village = "";
      copyData.stockies = stockiesList[0].username;
    }

    if (e.target.name === "village") {
      console.log(e.target.value);
      if (e.target.value) this.getStockiesList(e.target.value);
      else {
        this.setState({
          stockiesList: [{ username: "HQ001", name: "HEADQUARTER" }],
        });
      }
      copyData.stockies = stockiesList[0].username;
    }

    this.setState({ data: copyData });
  };

  onActivate = async () => {
    const { data } = this.state;
    if (data.password !== data.confirmPassword) {
      this.dangerAlert(
        "Oops, account activation failed with error: wrong confirm password"
      );
    } else {
      try {
        const res = await HttpService.post(ENDPOINT.ACTIVATE_USER, data, {
          "Content-Type": "application/json",
        });
        if (res.data && res.data.statusCode === 200) {
          this.successAlert("Account activation success");
        } else {
          this.dangerAlert(
            "Oops, account activation failed with error: " + res.data.message
          );
        }
      } catch (e) {
        this.dangerAlert("Oops, account activation failed with error: " + e);
      }
    }
  };

  successAlert = (message: string) => {
    const onConfirm = () => {
      this.hideAlert();
    };
    const onCancel = () => {
      this.hideAlert();
    };
    this.setState({
      alert: (
        <SuccessAlert
          message={message}
          onConfirm={onConfirm}
          onCancel={onCancel}
        />
      ),
    });
  };

  dangerAlert = (message: string) => {
    const onConfirm = () => {
      this.hideAlert();
    };
    const onCancel = () => {
      this.hideAlert();
    };
    this.setState({
      alert: (
        <DangerAlert
          message={message}
          onConfirm={onConfirm}
          onCancel={onCancel}
        />
      ),
    });
  };

  hideAlert = () => this.setState({ alert: null });

  render() {
    const {
      data,
      alert,
      stockiesList,
      provinceList,
      regencyList,
      districtList,
      villageList,
    } = this.state;
    return (
      <div className="app flex-row align-items-center">
        {alert}
        <Container>
          <Row className="justify-content-center">
            <Col>
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form>
                    <h1>Aktivasi Akun</h1>
                    <p className="text-muted">Isi sesuai data KTP Anda</p>
                    <Row>
                      <Col md="6" lg="6">
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i> &nbsp; Card ID
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            autoComplete="username"
                            name="username"
                            value={data.username}
                            onChange={this.onChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i> &nbsp; Card Key
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            autoComplete="key"
                            name="key"
                            value={data.key}
                            onChange={this.onChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i> &nbsp; Nama Lengkap
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            autoComplete="fullname"
                            name="fullname"
                            value={data.fullname}
                            onChange={this.onChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock"></i> &nbsp; Password
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="password"
                            autoComplete="password"
                            name="password"
                            value={data.password}
                            onChange={this.onChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock"></i> &nbsp; Konfirmasi
                              Password
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="password"
                            autoComplete="confirmPassword"
                            name="confirmPassword"
                            value={data.confirmPassword}
                            onChange={this.onChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i> &nbsp; Upliner ID
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            autoComplete="upliner"
                            name="upliner"
                            value={data.upliner}
                            onChange={this.onChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i> &nbsp; Sponsor ID
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            autoComplete="sponsor"
                            name="sponsor"
                            value={data.sponsor}
                            onChange={this.onChange}
                          />
                        </InputGroup>
                      </Col>
                      <Col md="6" lg="6">
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>@ &nbsp; Email</InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            autoComplete="email"
                            name="email"
                            value={data.email}
                            onChange={this.onChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              {" "}
                              <i className="icon-globe"></i> &nbsp; Alamat
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            autoComplete="address"
                            name="address"
                            value={data.address}
                            onChange={this.onChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-globe"></i> &nbsp; Provinsi
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="select"
                            autoComplete="province"
                            name="province"
                            value={data.province}
                            onChange={this.onChange}
                          >
                            {provinceList &&
                              provinceList.map((item: any) => {
                                return (
                                  <option value={item.id}>{item.name}</option>
                                );
                              })}
                          </Input>
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-globe"></i> &nbsp;
                              Kabupaten/Kota
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="select"
                            autoComplete="regency"
                            name="regency"
                            value={data.regency}
                            onChange={this.onChange}
                            disabled={data.province ? false : true}
                          >
                            {regencyList &&
                              regencyList.map((item: any) => {
                                return (
                                  <option value={item.id}>{item.name}</option>
                                );
                              })}
                          </Input>
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-globe"></i> &nbsp; Kecamatan
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="select"
                            autoComplete="district"
                            name="district"
                            value={data.district}
                            onChange={this.onChange}
                            disabled={data.regency ? false : true}
                          >
                            {districtList &&
                              districtList.map((item: any) => {
                                return (
                                  <option value={item.id}>{item.name}</option>
                                );
                              })}
                          </Input>
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-globe"></i> &nbsp;
                              Desa/Kelurahan
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="select"
                            autoComplete="village"
                            name="village"
                            value={data.village}
                            onChange={this.onChange}
                            disabled={data.district ? false : true}
                          >
                            {villageList &&
                              villageList.map((item: any) => {
                                return (
                                  <option value={item.id}>{item.name}</option>
                                );
                              })}
                          </Input>
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i> &nbsp; Stokis
                              Referensi
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="select"
                            autoComplete="stockies"
                            name="stockies"
                            value={data.stockies}
                            onChange={this.onChange}
                            disabled={data.village ? false : true}
                          >
                            {stockiesList &&
                              stockiesList.map((item: any) => {
                                return (
                                  <option value={item.username}>
                                    {item.username}-{item.name}
                                  </option>
                                );
                              })}
                          </Input>
                        </InputGroup>
                      </Col>
                    </Row>
                    <Button color="primary" block onClick={this.onActivate}>
                      Activate Account
                    </Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Activate;
