import React, { Component } from "react";

class Logout extends Component<any, any> {
  componentDidMount(){
    localStorage.clear();
    this.props.history.push('/login');
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
      </div>
    );
  }
}

export default Logout;