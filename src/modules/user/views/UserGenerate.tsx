import React, { Component } from 'react';
import { Button, Input } from 'reactstrap';
import { ENDPOINT } from '../../../utilities/api';
import { HttpService } from '../../../utilities/HttpService';
import { SuccessAlert, DangerAlert } from '../../../components/sweet-alert';
import QRCode from "react-qr-code";
import { exportComponentAsPNG } from "react-component-export-image";

class UserGenerate extends Component<any, any> {
  componentRef: any;
  constructor(props: any) {
    super(props);
    this.componentRef = React.createRef();
    this.state = {
      count: 0,
      generatedUser: [] as any[],
    }
  }

  successAlert = (message: string) =>{
    const onConfirm = () => { this.hideAlert() };
    const onCancel = () => { this.hideAlert() };
    this.setState({
      alert: 
      <SuccessAlert 
        message={message}
        onConfirm= {onConfirm}
        onCancel={onCancel}
      />
    })
  };

  dangerAlert = (message: string) =>{
    const onConfirm = () => { this.hideAlert() };
    const onCancel = () => { this.hideAlert() };
    this.setState({
      alert: 
      <DangerAlert 
        message={message}
        onConfirm= {onConfirm}
        onCancel={onCancel}
      />
    })
  };

  hideAlert = () => this.setState({alert: null});

  onClick = async () =>{
    const data = {
      count: this.state.count
    }
    try{
      const res = await HttpService.post(ENDPOINT.GENERATE_USER, data, {'Content-Type': 'application/json',});
      if(res.data && res.data.statusCode === 200){
        console.log(res)
        this.setState({generatedUser: res.data.message.user})
      }
      else{
        this.dangerAlert("Oops, add transaction failed with error: "+ res.data.message);
      }
    }
    catch(e){
      this.dangerAlert("Oops, account activation failed with error: "+e);

    }
  };

  render() {
    const {count, generatedUser} = this.state
    return (
      <div className="animated fadeIn">
        <Input value={count | 0} onChange={(e)=>this.setState({count: Number.parseInt(e.target.value)})}></Input>
        <br></br>
        <Button color="primary" onClick={this.onClick}>Generate User</Button>
        <br></br>
        
       {/* <ComponentToPrint ref={this.componentRef} /> */}
       <div ref={this.componentRef}>
       {/* <div> */}
        {
          generatedUser && generatedUser.map((user: { username: React.ReactNode; key: React.ReactNode; })=>{
            return(
            <div style={{height: '625px', marginBottom: '20px'}}>
              <img src="../../../../assets/img/card-back.png" alt="card"/>
              <div style={{marginTop: '-530px',marginLeft: '650px'}}>
                <h3>{user.username}</h3>
              </div>
              <div style={{marginTop: '50px', marginLeft: '650px'}}>
                <h3>{user.key}</h3>
              </div>
              <div style={{marginTop: '45px', marginLeft: '725px'}}>
                <QRCode value={user.username} size='160' />
              </div>
            </div>
          )})
        }
        </div>
        <button onClick={() => exportComponentAsPNG(this.componentRef)}>
           Export As PNG
       </button>
      </div>
    )
  }
}

export default UserGenerate;