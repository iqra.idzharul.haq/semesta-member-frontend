import React from "react";

const routes = [
  {
    path: "/monthly-level-bonus",
    exact: true,
    name: "Bonus Level",
    component: React.lazy(() => import("./views/MonthlyLevelBonusSummary")),
  },
];

export default routes;
