import React, { Component } from "react";
import { SearchBonus, BonusTable } from "../components";
import { getActiveMonthBonus } from "../helper/api";

class MonthlyPurchaseBonusSummary extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      filter: {
        period: "",
      },
      bonus: {},
    };
  }

  componentDidMount() {
    this.getActiveMonthBonusData();
  }

  async getActiveMonthBonusData() {
    const resBonus = await getActiveMonthBonus();
    if (!resBonus.error) {
      this.setState({ bonus: resBonus.data[0] });
    } else {
      console.log(resBonus.errorMessage);
    }
  }

  onSearch() {}

  onClear() {}

  setStateByName = (name: string, value: any) => {
    this.setState({ [name]: value });
  };

  render() {
    const { filter, bonus } = this.state;
    return (
      <div className="animated fadeIn">
        <SearchBonus
          onSearch={() => this.onSearch()}
          onClear={() => this.onClear()}
          filter={filter}
          setStateByName={(name: string, value: any) =>
            this.setStateByName(name, value)
          }
        />
        <BonusTable bonus={bonus} />
      </div>
    );
  }
}

export default MonthlyPurchaseBonusSummary;
