import { getHeader } from "../../../utilities/localStorage";
import { ENDPOINT } from "../../../utilities/api";
import { HttpService } from "../../../utilities/HttpService";

export const getLatestTransaction = async (stockies: any) =>{
  const data ={
    stockies: stockies,
    pageNumber: 0,
    pageSize: 7,
  }
  
  const headers = getHeader();
  try{
    const res = await HttpService.post(ENDPOINT.GET_TRANSACTION_BY_STOCKIES, data, headers);
    if(res.data && res.data.statusCode === 200){
      const result = {
        error: false,
        errorMessage: null,
        data: res.data.message,
      }
      return result;
    }
    else{
      const result = {
        error: true,
        errorMessage: res.data.message,
        data: [],
      }
      console.log(result);
      return result;
    }
  }
  catch(e){
    const result = {
      error: true,
      errorMessage: e,
      data: [],
    }
    console.log(result);
    return result;
  }
};

export async function getProduct(){
  const res = await HttpService.get(ENDPOINT.GET_PRODUCT, null, {'Content-Type': 'application/json',});

  if(res.data && res.data.statusCode === 200){
    return {
      error: false,
      message: res.data.message,
    }
  }
  return {
    error: true,
    messsage: "error when get all product excellence data",
  };
}