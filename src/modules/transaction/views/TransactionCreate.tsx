import React, { Component, Suspense } from 'react';
import { Button, Card, CardBody, Col, Form, Input, Row, Navbar, Nav } from 'reactstrap';
import { HttpService } from '../../../utilities/HttpService';
import { SuccessAlert, DangerAlert } from '../../../components/sweet-alert';
import { ENDPOINT } from '../../../utilities/api';
import { getLatestTransaction, getProduct } from '../helper/api';
import LatestTransaction from '../components/LatestTransaction';
import DashboardCard from '../components/StockiesCard';
import { formatCurrency } from '../../../utilities/helper';
import { getStockies } from '../../../utilities/localStorage';
import DefaultHeader from '../../../components/containers/DefaultLayout/DefaultHeader';
import {
  AppHeader,
} from '@coreui/react';

class TransactionCreate extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      data : {
        date: new Date().toISOString().slice(0,10),
        username: '',
        stockies: 'adcf9c28-5170-4cfc-873d-a788d4486e97',
        product: '6f78d68e-eb88-463f-af9f-79890cbcdee9',
        count: '',
      },
      stockies: {},
      latestTransactions: [],
      productList: [],
      totalTransactions: 0,
      alert: null,
    };
  };

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  async componentDidMount(){
    const stockies = await getStockies()
    const {data} = this.state
    if(!stockies){
      this.props.history.push('/')
    }else{
      data.stockies = stockies.id
      this.setState({data, stockies});
      this.getTransactionData();
      this.getProductList();
    }
  }

  async getProductList(){
    const {data} = this.state
    const res = await getProduct();
    if(!res.error){
      data.product = res.message[0].id;
      this.setState({productList: res.message, data});
    }else{
      this.setState({productList: []});
    }
  }


  async getTransactionData(){
    const res = await getLatestTransaction(this.state.data.stockies);
    if(!res.error){
      this.setState({latestTransactions: res.data.content});
      this.setState({totalTransactions: res.data.totalElements});
    }else{
      this.setState({latestTransactions: []});
    }
  }

  onChange = (e: any) =>{
    const { data } = this.state;
    const copyData = {...data};
    let value = e.target.value;
    if(e.target.name === 'username' || e.target.name === 'stockies') value = value.toUpperCase();
    copyData[e.target.name] = value;
    this.setState({data: copyData});
  };

  onClick = async () =>{
    const { data } = this.state;
    try{
      const res = await HttpService.post(ENDPOINT.CREATE_TRANSACTION, data, {'Content-Type': 'application/json',});
      if(res.data && res.data.statusCode === 200){
        this.successAlert(res.data.message);
      }
      else{
        console.log(res)
        this.dangerAlert("Oops, add transaction failed with error: "+ res.data.message);
      }
    }
    catch(e){
      this.dangerAlert("Oops, add transaction failed with error: "+e);

    }
  };

  successAlert = (message: string) =>{
    const onConfirm = () => { 
      this.hideAlert() 
      this.getTransactionData()
    };
    const onCancel = () => { this.hideAlert() };
    this.setState({
      alert: 
      <SuccessAlert 
        message={message}
        onConfirm= {onConfirm}
        onCancel={onCancel}
      />
    })
  };

  dangerAlert = (message: string) =>{
    const onConfirm = () => { this.hideAlert() };
    const onCancel = () => { this.hideAlert() };
    this.setState({
      alert: 
      <DangerAlert 
        message={message}
        onConfirm= {onConfirm}
        onCancel={onCancel}
      />
    })
  };

  hideAlert = () => this.setState({alert: null});

  signOut() {
    localStorage.clear();
    this.props.history.push('/login');
  }

  render() {
    const {data, alert, latestTransactions, totalTransactions, productList, stockies} = this.state;
    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            <DefaultHeader onLogout={() => this.signOut()} />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <div className="main mx-4 pad-top-20">
            {alert}
            <Row>
              <Col>
                <DashboardCard title="TOTAL STOCK" value={stockies.currentStock+" KG"} icon="fa fa-archive fa-2x align-self-flex-end" primary={false}/>
              </Col>
              <Col>
                <DashboardCard title="TOTAL TRANSACTION" value={totalTransactions} icon="fa fa-pencil fa-2x align-self-flex-end" primary={false}/>
              </Col>
              <Col>
                <DashboardCard title="INCOME" value={ formatCurrency(100)} icon="fa fa-money fa-2x align-self-flex-end" primary={true}/>
              </Col>
            </Row>
            <Row>
              <Col lg="5">
                <Card>
                  <CardBody>
                    <Form>
                      <h2>New Transaction</h2>
                      <p className="text-muted">Add new transaction</p>
                      <Row className="mb-3">
                        <Col>
                            DATE
                        </Col>
                        <Col  xs="8" sm="8" md="8" lg="8">
                          <Input type="date" autoComplete="date" name="date" value={data.date} onChange={this.onChange}/>
                        </Col>
                      </Row>
                      <Row className="mb-3">
                        <Col>
                            BUYER
                        </Col>
                        <Col  xs="8" sm="8" md="8" lg="8">
                          <Input type="text" autoComplete="username" name="username" value={data.username} onChange={this.onChange}/>
                        </Col>
                      </Row>
                      <Row className="mb-3">
                        <Col>
                            STOCKIES
                        </Col>
                        <Col  xs="8" sm="8" md="8" lg="8">
                          <Input type="select" autoComplete="stockies" name="stockies" value={data.stockies} onChange={this.onChange} disabled>
                            <option value={data.stockies}>{getStockies()? getStockies().name : "STOCKIES NAME"}</option>
                          </Input>
                        </Col>
                      </Row>
                      <Row className="mb-3">
                        <Col>
                            PRODUCT
                        </Col>
                        <Col  xs="8" sm="8" md="8" lg="8">
                          <Input type="select" autoComplete="product" name="product" value={data.product} onChange={this.onChange}>
                            {
                              productList && productList.map((item: any)=>{
                                return(
                                  <option value={item.id}>{item.name} {item.weight} Kg</option>
                                )
                              })
                            }
                          </Input>
                        </Col>
                      </Row>
                      <Row className="mb-3">
                        <Col>
                            COUNT
                        </Col>
                        <Col  xs="8" sm="8" md="8" lg="8">
                          <Input type="number" autoComplete="count" name="count" value={data.count} onChange={this.onChange}/>
                        </Col>
                      </Row>
                      <Button color="primary" block onClick={this.onClick}>Add Transaction</Button>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
              <Col lg="7">
                <LatestTransaction latestTransactions={latestTransactions}/>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

export default TransactionCreate;
