import React, { Component} from 'react';
import { Card, CardBody, Row, Col, Label } from 'reactstrap';

class DashboardCard extends Component<any, any> {
  render() {
    const { title, value, icon, primary} = this.props
    return (
      <Card className={primary? "dashboard-card-primary": "dashboard-card-secondary"}>
        <CardBody>
          <Row>
            <Col>
              <Label>{title}</Label>
            <h5 className="bolder">{value}</h5>
            </Col>
          </Row>
        </CardBody>
      </Card>
    );
  }
}

export default DashboardCard;
